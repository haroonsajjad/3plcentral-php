<?php
require_once dirname( __FILE__ ) . '/lib/3plcentral-soap.php';
require_once dirname( __FILE__ ) . '/lib/3plcentral-whoami.php';
require_once dirname( __FILE__ ) . '/lib/3plcentral-createitem.php';
require_once dirname( __FILE__ ) . '/lib/3plcentral-createorder.php';
/**
 * ThreePLCentral_PHP
 */
class ThreePLCentral_PHP
{
	private $config;

    public function __construct( $config = array() )
    {
        if( !is_array( $config ) || count( $config ) <= 0 )
        {
        	throw new Exception("Inavlid Access: Configuration array must be provided to initialize 3PL Central PHP class." );
        	return false;
        }

        $validArgsList = array( 'ThreePLKey', 'ThreePLID', 'Login', 'Password', 'FacilityID', 'CustomerID' );

        foreach ($config as $key => $value)
        {
        	if( !in_array( $key, $validArgsList ) )
        	{
        		throw new Exception('Invalid Arguments Passed: Arguments should be ' . implode( ', ', $validArgsList ) );
        		break;
        	}

        	if( empty( $value ) )
        	{
        		throw new Exception('Invalid Arguments Values: All arguments values must be passed.' );
        		break;
        	}
        }

        $this->config = $config;

        return $this;
    }

    public function getSOAPFunctions()
    {
    	$soap = new ThreePLCentral_SOAP();
    	return $soap->__getFunctions();
    }

    public function whoAmI()
    {
    	$whoami = new ThreePLCentral_WhoAmI();
    	return $whoami->getResponse();
    }

    public function createItem( Array $itemsArray )
    {
    	$extLoginData = $this->config;
    	$createItemObj = new ThreePLCentral_CreateItem( $extLoginData, $itemsArray );
    	return $createItemObj->getResponse();
    }

    public function createOrder( Array $ordersArray )
    {
    	$extLoginData = $this->config;
    	$createOrderObj = new ThreePLCentral_CreateOrder( $extLoginData, $ordersArray );
    	return $createOrderObj->getResponse();
    }
}