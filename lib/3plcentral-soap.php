<?php

class ThreePLCentral_SOAP extends SoapClient
{
	const WSDL_URL = 'https://secure-wms.com/webserviceexternal/contracts.asmx?wsdl';
	const SOAP_URL = 'http://www.JOI.com/schemas/ViaSub.WMS/';

	public function __construct()
	{
		$opts = array(
			'ssl' => array(
				'ciphers'=>'RC4-SHA',
				'verify_peer'=>false,
				'verify_peer_name'=>false
			)
		);
		$params = array (
			'encoding' => 'UTF-8',
			'verifypeer' => false,
			'verifyhost' => false,
			'soap_version' => SOAP_1_2,
			'trace' => 1,
			'exceptions' => 1,
			"connection_timeout" => 180,
			'stream_context' => stream_context_create($opts)
		);
		parent::__construct( self::WSDL_URL, $params );
	}
}