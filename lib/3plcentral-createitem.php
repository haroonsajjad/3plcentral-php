<?php

class ThreePLCentral_CreateItem extends ThreePLCentral_SOAP
{
	private $itemsArray;
	private $extLoginData;

	public function __construct( Array $extLoginData, Array $itemsArray )
	{
		parent::__construct();

		if( !is_array( $itemsArray ) || count( $itemsArray ) <= 0 )
        {
        	throw new Exception("Invalid Access: To create new item, please provide arguments in array format." );
        	return false;
        }

        $this->extLoginData = $extLoginData;
        $this->itemsArray = $itemsArray;
	}

	public function getResponse()
	{
		$arguments = array(
			'extLoginData'	=> (object) $this->extLoginData,
			'items'			=> (object) $this->itemsArray
		);
		return $this->__SoapCall( 'CreateItems', $arguments );
	}
}