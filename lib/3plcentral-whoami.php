<?php

class ThreePLCentral_WhoAmI extends ThreePLCentral_SOAP
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getResponse()
	{
		$whoami['WhoAmI'] = parent::SOAP_URL;
		return $this->__SoapCall( 'WhoAmI', $whoami );
	}
}