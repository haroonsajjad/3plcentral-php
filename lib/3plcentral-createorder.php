<?php

class ThreePLCentral_CreateOrder extends ThreePLCentral_SOAP
{
	private $ordersArray;
	private $extLoginData;

	public function __construct( Array $extLoginData, Array $ordersArray )
	{
		parent::__construct();

		if( !is_array( $ordersArray ) || count( $ordersArray ) <= 0 )
        {
        	throw new Exception("Invalid Access: To create new item, please provide arguments in array format." );
        	return false;
        }

        $this->extLoginData = $extLoginData;
        $this->ordersArray = $ordersArray;
	}

	public function getResponse()
	{
		$arguments = array(
			'extLoginData'	=> (object) $this->extLoginData,
			'items'			=> (object) $this->ordersArray
		);
		return $this->__SoapCall( 'CreateOrders', $arguments );
	}
}