<?php

/**
 * Create Item
 */

require_once '../3plcentral.php';

try {
	$config = array(
		'ThreePLKey'	=> '{ae89686b-e927-4abc-8c60-c340995f9b72}',
		'ThreePLID'		=> '479',
		'Login'			=> 'organicoliviaapi',
		'Password'		=> 'organicoliviaapi',
		'FacilityID'	=> '1',
		'CustomerID'	=> '17'
	);
	$threepl = new ThreePLCentral_PHP( $config );

	$items = array(
		array(
			'SKU' => 'ABC1230', //
			'Description' => 'Test Item 0', //
			'Description2' => 'Test Item 0 2', //
			'CustomerID' => '17', //
			/*'Min' => '25', // [OPTIONAL]
			'Max' => '1000', // [OPTIONAL]
			'ReorderQty' => '100', // [OPTIONAL]
			'CycleCount' => '30', // [OPTIONAL]
			'InventoryMethod' => 'LIFO', // [Default is FIFO]
			'Cost' => '10', // [OPTIONAL]
			'UPC' => '12345678912', // [OPTIONAL]
			'Temperature' => '25', //[OPTIONAL]
			'IsTrackLotNumber' => 'true', // [OPTIONAL – Default is False]
			'IsTrackLotNumberRequired' => 'true', // [OPTIONAL – Default is False]
			'IsTrackSerialNumber' => 'true', // [OPTIONAL – Default is False]
			'IsTrackSerialNumberRequired' => 'true', // [OPTIONAL – Default is False]
			'IsTrackSerialNumberUnique' => 'true', // [OPTIONAL – Default is False]
			'IsTrackExpirationDate' => 'true', // [OPTIONAL – Default is False]
			'IsTrackExpirationDateRequired' => 'true', //[OPTIONAL – Default is False]
			'IsTrackCost' => 'true', // [OPTIONAL – Default is False]
			'IsTrackCostRequired' => 'true', // [OPTIONAL – Default is False]
			'InventoryUnitOfMeasure' => 'Each', //
			'LabelingUnitOfMeasure' => 'Carton', //
			'InventoryUnitsPerLabelingUnit' => '12', //
			'MeasurementSystem' => 'USImperial or Metric', // [Default is US Imperial]
			'LabelingUnitLength' => '12', //4
			'LabelingUnitWidth' => '14', //
			'LabelingUnitHeight' => '16', //
			'LabelingUnitWeight' => '5', //
			'MovableUnitType' => 'string', // [OPTIONAL]
			'MovableUnitLength' => '10.00', // [OPTIONAL]
			'MovableUnitWidth' => '10.00', // [OPTIONAL]
			'MovableUnitHeight' => '10.00', // [OPTIONAL]
			'MovableUnitWeight' => '10.00', // [OPTIONAL]
			'MovableUnitTie' => '10.00', // [OPTIONAL]
			'MovableUnitHigh' => '10.00', // [OPTIONAL]
			'MovableUnitQty' => '10.00', // [OPTIONAL]
			'IsHazMat' => 'false', // [OPTIONAL]
			'HazMatID' => 'string', // [OPTIONAL]
			'HazMatShippingName' => 'string', // [OPTIONAL]
			'HazMatHazardClass' => 'string', // [OPTIONAL]
			'HazMatPackingGroup' => 'Default or I or II or III', // [OPTIONAL]
			'HazMatFlashPoint' => 'string', //[OPTIONAL]
			'HazMatLabelCode' => 'string', // [OPTIONAL]
			'HazMatFlag' => 'Default or X or RQ', // [OPTIONAL]*/
		)
	);

	echo '<pre>' . print_r( $threepl->createItem( $items ), true ) . '</pre>';
}
catch( Exception $e ) {
	echo $e->getMessage();
}