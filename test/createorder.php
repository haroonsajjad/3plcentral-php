<?php

/**
 * Create Order Test
 */

require_once '../3plcentral.php';

try {
	$config = array(
		'ThreePLKey'	=> '{ae89686b-e927-4abc-8c60-c340995f9b72}',
		'ThreePLID'		=> '479',
		'Login'			=> 'organicoliviaapi',
		'Password'		=> 'organicoliviaapi',
		'FacilityID'	=> '1',
		'CustomerID'	=> '17'
	);
	$threepl = new ThreePLCentral_PHP( $config );

	$orders = array(
		array(
			'TransInfo' => array(
				'ReferenceNum' => 'TestOrder123',
				// 'EarliestShipDate' => '20160401', // [OPTIONAL]
				// 'ShipCancelDate' => '20160430', // [OPTIONAL]
				// 'PONum' => 'TestPO' // [OPTIONAL]
			),
			'ShipTo' => array(
				'Name' => 'John Smith', // [OPTIONAL]
				'CompanyName' => 'John\'s Bakery',
				'Address' => array(
					'Address1' => '1212 Main Street',
					// 'Address2' => 'string', //[OPTIONAL]
					'City' => 'Los Angeles',
					'State' => 'CA',
					'Zip' => '90010',
					'Country' => 'US'
				),
				// 'PhoneNumber1' => 'string', // [OPTIONAL]
				// 'Fax' => 'string', // [OPTIONAL]
				// 'EmailAddress1' => 'string', // [OPTIONAL]
				// 'CustomerName' => 'string', // [OPTIONAL]
				// 'Vendor' => 'string', // [OPTIONAL]
				// 'Dept' => 'string', // [OPTIONAL]
				// 'RetailerID' => 'int', // [OPTIONAL – do not send if not matching with value in the 3PL				Central]
			),
			'ShippingInstructions' => array(
				'Carrier' => 'UPS',
				'Mode' => 'Ground',
				'BillingCode' => 'FreightCollect',
				'Account' => '12345675',
				'ShippingNotes' => 'Shipping test notes.' // [OPTIONAL]
			),
			/*'ShipmentInfo' => array(
				'NumUnits1' => 'decimal', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
				'NumUnits1TypeID' => 'int', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
				'NumUnits1TypeDesc' => 'string', // [OPTIONAL – do not send if not matching	with value in the 3PL Central]
				'NumUnits2' => 'decimal', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
				'NumUnits2TypeID' => 'int', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
				'NumUnits2TypeDesc' => 'string', // [OPTIONAL – do not send if not matching	with value in the 3PL Central]
				'TotalWeight' => 'decimal', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
				'TotalVolume' => 'decimal' // [OPTIONAL – do not send if not matching with value in the	3PL Central]
			),*/
			'Notes' => 'Test Order Notest',
			// 'PalletCount' => 'int', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
			'OrderLineItems' => array(
				'OrderLineItem' => array(
					'SKU' => 'TestItem',
					// 'Qualifier' => 'string', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
					// 'Qty' => '12',
					// 'Packed' => 'decimal', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
					// 'CuFtPerCarton' => 'decimal', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
					// 'LotNumber' => 'string', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
					// 'SerialNumber' => 'string', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
					// 'ExpirationDate' => 'dateTime', // [OPTIONAL – do not send if not matching with value in the 3PL Central]
					// 'Notes' => 'string', // [OPTIONAL]
					// 'FulfillmentSalePrice' => 'decimal', // [OPTIONAL]
					// 'FulfillmentDiscountPercentage' => 'decimal', // [OPTIONAL]
					// 'FulfillmentDiscountAmount' => 'decimal' // [OPTIONAL]
				)
			),
			/*'SavedElements' => array( // [ENTIRE SEGMENT IS OPTIONAL]
				'CodeDescrPair' => array(
					'Code' => 'string',
					'Description' => 'string'
				),
				'CodeDescrPair' => array(
					'Code' => 'string',
					'Description' => 'string'
				)
			),*/
			/*'FulfillmentInfo' => array( // [ENTIRE SEGMENT IS OPTIONAL]
				'FulfillInvShippingAndHandling' => 'decimal',
				'FulfillInvTax' => 'decimal',
				'FulfillInvDiscountCode' => 'string',
				'FulfillInvDiscountAmount' => 'decimal',
				'FulfillInvGiftMessage' => 'string'
			),*/
			/*'SoldTo' => array( // [ENTIRE SEGMENT IS OPTIONAL]
				'Name' => 'string',
				'CompanyName' => 'string',
				'Address' => array(
					'Address1' => 'string',
					'Address2' => 'string',
					'City' => 'string',
					'State' => 'string',
					'Zip' => 'string',
					'Country' => 'string'
				),
				'PhoneNumber1' => 'string',
				'Fax' => 'string',
				'EmailAddress1' => 'string',
				'CustomerName' => 'string',
				'Vendor' => 'string',
				'Dept' => 'string',
				'RetailerID' => 'int'
			)*/
		)
	);

	echo '<pre>' . print_r( $threepl->createOrder( $orders ), true ) . '</pre>';
}
catch( Exception $e ) {
	echo $e->getMessage();
}